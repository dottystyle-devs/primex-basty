<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\Stock;
use Tests\TestCase;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use Tests\Concerns\HasSamplePayload;
use Tests\Contracts\ModelTestContract;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StockTest extends TestCase implements ModelTestContract
{
    use HasSamplePayload;

    /**
     * Execute test in invalid format.
     */
    public function testInvalidDataFormat()
    {
        $this->assertTrue(true);
    }

    /**
     * Execute test in newly created data.
     */
    public function testValidDataCreation()
    {
        $stockPayload = $this->sampleStockPayload();

        $product = Product::create($this->sampleProductPayload());

        $product->stocks()->create($stockPayload);

        $this->assertDatabaseHas('stocks', $stockPayload);

        $this->json('post', "api/v1/products/$product->id/stock", $stockPayload)
            ->assertStatus(Response::HTTP_OK);
    }

    /**
     * Execute test in viewing data.
     */
    public function testValidDataViewing()
    {
        $this->assertTrue(true);
    }

    /**
     * Execute test in deleting data.
     */
    public function testValidDataDeletion()
    {
        $this->assertTrue(true);
    }

    /**
     * Execute test in updating data.
     */
    public function testValidDataModification()
    {
        $this->assertTrue(true);
    }
}
