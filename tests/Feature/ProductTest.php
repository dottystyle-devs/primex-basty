<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Product;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use Tests\Concerns\HasSamplePayload;
use Tests\Contracts\ModelTestContract;

class ProductTest extends TestCase implements ModelTestContract
{
    use HasSamplePayload;

    /**
     * Execute test in invalid format.
     */
    public function testInvalidDataFormat()
    {
        $this->json('get', 'api/v1/products')
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'data' => [
                        '*' => [
                            'id',
                            'code',
                            'name',
                            'description',
                            'created_at',
                            'onHand_stocks',
                            'available_stocks'
                        ]
                    ]
                ]
            );
    }

    /**
     * Execute test in newly created data.
     */
    public function testValidDataCreation()
    {
        $payload = $this->sampleProductPayload();

        $this->json('post', 'api/v1/products', $payload)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'code',
                        'name',
                        'description',
                        'created_at',
                    ]
                ]
            );

        // Make sure the product exists in the database
        $this->assertDatabaseHas('products', $payload);
    }

    /**
     * Execute test in viewing data.
     */
    public function testValidDataViewing()
    {
        $product = Product::create($this->sampleProductPayload());

        $this->json('get', "api/v1/products/$product->id")
            ->assertStatus(Response::HTTP_OK)
            ->assertExactJson(
                [
                    'data' => [
                        'id' => $product->id,
                        'code' => $product->code,
                        'name' => $product->name,
                        'description' => $product->description,
                        'created_at' => $product->created_at,
                        'stocks' => []
                    ]
                ]
            );
    }

    /**
     * Execute test in deleting data.
     */
    public function testValidDataDeletion()
    {
        $payload = $this->sampleProductPayload();
        $product = Product::create($payload);

        $this->json('delete', "api/v1/products/$product->id")
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'code',
                    'message'
                ]
            );

        // Make sure the product does not exists in the database
        $this->assertSoftDeleted('products', $payload);
    }

    /**
     * Execute test in updating data.
     */
    public function testValidDataModification()
    {
        $product = Product::create(
            $this->sampleProductPayload()
        );

        $payload = $this->sampleProductPayload();

        $this->json('put', "api/v1/products/$product->id", $payload)
            ->assertStatus(Response::HTTP_OK)
            ->assertExactJson(
                [
                    'data' => [
                        'id' => $product->id,
                        'code' => $payload['code'],
                        'name' => $payload['name'],
                        'description' => $payload['description'],
                        'created_at' => $product->created_at,
                    ]
                ]
            );
    }
}
