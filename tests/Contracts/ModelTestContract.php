<?php

namespace Tests\Contracts;

interface ModelTestContract
{
    /**
     * Execute test in invalid format.
     */
    public function testInvalidDataFormat();

    /**
     * Execute test in newly created data.
     */
    public function testValidDataCreation();

    /**
     * Execute test in viewing data.
     */
    public function testValidDataViewing();

    /**
     * Execute test in deleting data.
     */
    public function testValidDataDeletion();

    /**
     * Execute test in updating data.
     */
    public function testValidDataModification();
}
