<?php

namespace Tests\Concerns;

use Illuminate\Support\Str;

trait HasSamplePayload
{
    /**
     * Generate a valid test data.
     */
    public function sampleProductPayload()
    {
        $code = Str::random(16);

        return [
            'code' => $code,
            'name' => "Product {$code}",
            'description' => "This Product {$code} is awesome!"
        ];
    }

    /**
     * Generate a valid test data.
     */
    public function sampleStockPayload()
    {
        $onHand = $this->faker->numberBetween(0, 5);

        return [
            'on_hand' => $onHand,
            'taken' => $this->faker->numberBetween(0, $onHand),
            'production_date' => $this->faker->dateTimeThisMonth(),
        ];
    }
}
