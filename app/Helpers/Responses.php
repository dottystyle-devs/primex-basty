<?php

if (! function_exists('successResponse')) {
    /**
     * Return a new response from the application.
     *
     * @param  \Illuminate\Contracts\View\View|string|array|null  $content
     * @param  int  $status
     * @param  array  $headers
     * @return \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    function successResponse(string $message, $status = 200)
    {
        return response()->json([
            'code' => $status,
            'message' => $message
        ]);
    }
}

if (! function_exists('dataSuccessResponse')) {
    /**
     * Return a new response from the application.
     *
     * @param  \Illuminate\Contracts\View\View|string|array|null  $content
     * @param  int  $status
     * @param  array  $headers
     * @return \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    function dataSuccessResponse(string $message, array $data,  $status = 200)
    {
        return response()->json([
            'code' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }
}

if (! function_exists('failedResponse')) {
    /**
     * Return a new response from the application.
     *
     * @param  \Illuminate\Contracts\View\View|string|array|null  $content
     * @param  int  $status
     * @param  array  $headers
     * @return \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    function failedResponse(string $message, $status = 500)
    {
        return response()->json([
            'code' => $status,
            'message' => $message
        ]);
    }
}
