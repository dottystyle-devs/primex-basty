<?php

namespace App\Http\Middleware;

use Closure;

class Paginated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $perPage = (int) $request->query('per_page', 10);
        $perPage = ($perPage > 1) ? $perPage : 10;
        $request->query->set('per_page', $perPage);

        $order = $request->query('order');
        $order = ($order != 'desc') ? 'asc' : 'desc';
        $request->query->set('order', $order);

        return $next($request);
    }
}
