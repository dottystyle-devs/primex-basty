<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Stock;
use App\Models\Product;
use App\Imports\StocksImport;
use App\Services\FileService;
use App\Http\Requests\FileImportRequest;
use App\Http\Requests\Stock\CreateRequest;
use App\Http\Resources\Stocks\TableResource as StockTableResource;
use App\Http\Resources\Stocks\DetailResource as StockDetailResource;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $stocks = Stock::sorts($request->sorts)->get()
            ->requestFilters($request->filters)
            ->paginate($request->per_page);

        return StockTableResource::collection($stocks);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Request\Stock\CreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request, Product $product)
    {
        $stock = $product->stocks()->create($request->validated());

        return new StockDetailResource($stock);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Stock $stock)
    {
        return new StockDetailResource($stock);
    }

    /**
     * Imports resource in storage.
     *
     * @param  \App\Http\Requests\FileImportRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function import(FileImportRequest $request)
    {
        $data = app(FileService::class, [StocksImport::class])->import($request->file('file'));

        return dataSuccessResponse(
            __('Successfully imported data.'),
            $data,
        );
    }
}
