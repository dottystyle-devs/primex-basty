<?php

namespace App\Http\Controllers;

use App\Models\Stock;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Services\FileService;
use App\Imports\ProductsImport;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use App\Http\Resources\ProductResource;
use App\Http\Requests\FileImportRequest;
use App\Http\Requests\Product\CreateRequest;
use App\Http\Requests\Product\UpdateRequest;
use App\Http\Resources\Stocks\DetailResource as StockDetailResource;
use App\Http\Resources\Products\TableResource as ProductTableResource;
use App\Http\Resources\Products\DetailResource as ProductDetailResource;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('stock_id')) {
            return new StockDetailResource(Stock::find($request->stock_id));
        }

        $products = Product::sorts($request->sorts)->get()
            ->requestFilters($request->filters)
            ->paginate($request->per_page);

        return ProductTableResource::collection($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Product\CreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $product = Product::create($request->validated());

        return new ProductResource($product);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Product $product)
    {
        if ($request->has('stock_id')) {
            return new StockDetailResource(Stock::find($request->stock_id));
        }

        return new ProductDetailResource($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Product\UpdateRequest  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Product $product)
    {
        $product->update($request->validated());

        return new ProductResource($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return successResponse(__('Successfully deleted.'));
    }

    /**
     * Imports resource in storage.
     *
     * @param  \App\Http\Requests\FileImportRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function import(FileImportRequest $request)
    {
        $data = app(FileService::class, [ProductsImport::class])->import($request->file('file'));

        return dataSuccessResponse(
            __('Successfully imported data.'),
            $data,
        );
    }
}
