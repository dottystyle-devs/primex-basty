<?php

namespace App\Http\Resources\Stocks;

use Illuminate\Http\Resources\Json\JsonResource;

class TableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'on_hand' => $this->on_hand,
            'taken' => $this->taken,
            'description' => $this->description,
            'created_at' => $this->created_at,
        ];
    }
}
