<?php

namespace App\Http\Resources\Stocks;

use App\Http\Resources\ProductResource as ProductDetailResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'on_hand' => $this->on_hand,
            'taken' => $this->taken,
            'production_date' => $this->production_date,
            'created_at' => $this->created_at,
            'product' => new ProductDetailResource($this->product)
        ];
    }
}
