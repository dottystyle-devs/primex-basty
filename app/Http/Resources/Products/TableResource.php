<?php

namespace App\Http\Resources\Products;

use App\Http\Resources\StockResource;
use Illuminate\Http\Resources\Json\JsonResource;

class TableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'description' => $this->description,
            'created_at' => $this->created_at,
            'onHand_stocks' => $this->stocks->count(),
            'available_stocks' => $this->available_stocks,
            'stocks' => StockResource::collection($this->stocks)
        ];
    }
}
