<?php

namespace App\Models;

use App\Models\Concerns\HasSortable;
use App\Models\Concerns\HasCustomCollection;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes, HasCustomCollection, HasSortable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'code',
        'name',
        'description',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['deleted_at'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
        'deleted_at' => 'datetime:Y-m-d',
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'stocks'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['available_stocks', 'onhand_stocks'];

    /**
     * Determine if the product is available.
     *
     * @return int
     */
    public function getOnhandStocksAttribute()
    {
        return $this->stocks->count();
    }

    /**
     * Determine if the product is available.
     *
     * @return int
     */
    public function getAvailableStocksAttribute()
    {
        return $this->stocks->filter(function($stock) {
            return $stock->on_hand !== $stock->taken;
        })->count();
    }

    /**
     * Get the stocks associated with the product.
     */
    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }
}
