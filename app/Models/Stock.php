<?php

namespace App\Models;

use App\Models\Concerns\HasSortable;
use App\Models\Concerns\HasCustomCollection;

class Stock extends Model
{
    use HasCustomCollection, HasSortable;
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'on_hand',
        'taken',
        'production_date',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['deleted_at'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'production_date' => 'datetime',
    ];

    /**
     * Get the products of the stock.
     */
    public function product()
    {
        return $this->BelongsTo(Product::class);
    }
}
