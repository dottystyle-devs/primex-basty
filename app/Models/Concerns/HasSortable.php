<?php

namespace App\Models\Concerns;

use Illuminate\Support\Str;

trait HasSortable
{
    /**
     * Apply the scope to scope sorting multiple fields.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  array  $sorts
     * @return \Illuminate\Database\Eloquent\Builder
     * @return void
     */
    public function scopeSorts($query, $sorts)
    {
        if (!$sorts || !is_array($sorts)) {
            return;
        }

        foreach ($sorts as $field => $direction){
            $related = explode(".", $field);

            if (count($related) > 1) {
                $relTable = $related[0];
                $table = Str::singular($this->getTable());

                $query->join($relTable, "{$relTable}.{$table}_id", "{$this->getTable()}.id");
            } else {
                $table = $this->getTable();
                $field = "{$table}.{$field}";
            }

            $query->orderBy($field, $direction);
        }
    }
}
