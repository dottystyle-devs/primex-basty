<?php

namespace App\Models\Concerns;

use App\Models\Collections\CustomCollection;
use App\Models\Collections\CollectionBuilder;

trait HasCustomCollection
{
    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param  \Illuminate\Database\Query\Builder $builder
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function newEloquentBuilder($builder)
    {
        return new CollectionBuilder($builder);
    }

    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param  \Illuminate\Database\Query\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function newCollection(array $models = [])
    {
        return new CustomCollection($models);
    }
}
