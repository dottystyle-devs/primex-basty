<?php

namespace App\Models\Collections;

use Illuminate\Database\Eloquent\Builder;

class CollectionBuilder extends Builder
{
    /**
     * Get the date of the given collection.
     *
     * @return self
     */
    public function whereDate($column, $value)
    {
        $this->where($column, $value);

        return $this;
    }
}
