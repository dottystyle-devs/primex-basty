<?php

namespace App\Models\Collections;

use Illuminate\Database\Eloquent\Collection;

class CustomCollection extends Collection
{
    /**
     * Filter based on the request.
     *
     * @param $filters (optional)
     * @return self
     */
    public function requestFilters($filters = [])
    {
        if (! $filters) {
            return $this;
        }

        return $this->filter(function($value) use ($filters) {
            return $filters == $value->only(array_keys($filters));
        });
    }

    /**
     * Sorts based on the request.
     *
     * @param $sorts (optional)
     * @param string $direction
     * @return self
     */
    public function requestSorts($sorts = '')
    {
        if (! $sorts) {
            return $this;
        }

        $columns = explode(',', $sorts);

        $collection = collect($columns)->map(function ($item) {
            $column = $item;
            $direction = 'asc';

            if ($item[0] === '-') {
                $column = substr($item, 1);
                $direction = 'desc';
            }

            return [$column, $direction];
        })->values()->all();

        return $this->sortBy($collection);
    }
}
