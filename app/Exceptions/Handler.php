<?php

namespace App\Exceptions;

use Exception;
use Throwable;
use Illuminate\Database\QueryException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Exceptions\PostTooLargeException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
	 * Redirect errors for API requests to renderForApi(). Otherwise, the regular renderer from the base class is used.
	 *
	 * @param Request $request
	 * @param Throwable $e
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @throws Throwable
	 */
	public function render($request, Throwable $e)
	{
		if ($request->is("api/*")) {
			return $this->renderForApi($request, $e);
		}

		return parent::render($request, $e);
	}

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function renderForApi($request, Throwable $error)
    {
        if ($error instanceof NotFoundHttpException) {
            return $this->response(new Exception('Not Found.'), 404);
        }

        if ($error instanceof ModelNotFoundException) {
            return $this->response(new Exception('Model Not Found.'), 405);
        }

        if ($error instanceof AuthenticationException) {
            return $this->response($error, 401);
        }

        if ($error instanceof AuthorizationException) {
            return $this->response($error, 403);
        }

        if ($error instanceof MethodNotAllowedHttpException) {
            return $this->response(new Exception('Method Not Allowed.'), 405);
        }

        if ($error instanceof PostTooLargeException) {
            return $this->response(new Exception('Post Too Large.'), 413);
        }

        if ($error instanceof QueryException) {
            return $this->response($error, 500);
        }

        return failedResponse($error->getMessage(), $error->getCode());

        // return parent::render($request, $exception);
    }

    protected function response(Exception $exception, int $statusCode = 500)
    {
        return failedResponse($exception->getMessage(), $statusCode);
    }
}
