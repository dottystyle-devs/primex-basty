<?php

namespace App\Services;

use App\Services\Contracts\FileContract;
use Illuminate\Http\UploadedFile;

class FileService implements FileContract
{
    protected $importClass;

    /**
     * @param string $importClass
     */
    public function __construct($importClass)
    {
        $this->importClass = $importClass;
    }

    /**
     * Process on importing data.
     *
     * @param \Illuminate\Http\UploadedFile $file
     * @return array $result
     */
    public function import(UploadedFile $file)
    {
        $importer = new $this->importClass;
        $importer->import($file);

        return $importer->getPostData();
    }
}
