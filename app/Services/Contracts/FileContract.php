<?php

namespace App\Services\Contracts;

use Illuminate\Http\UploadedFile;

interface FileContract
{
    /**
     * @param \Illuminate\Http\UploadedFile $file
     */
    public function import(UploadedFile $file);
}
