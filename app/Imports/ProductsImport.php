<?php

namespace App\Imports;

class ProductsImport extends CsvImport
{
    /**
     * Set the model reference.
     */
    public $model = 'App\\Models\\Product';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'id' => ['required', 'integer'],
            'code' => ['required', 'max:255'],
            'name' => ['required', 'max:255'],
            'description' => ['required', 'max:255'],
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function validationMessages(): array
    {
        return [
            'code.required' => __('Code is required.'),
            'name.required' => __('Name is required.'),
            'description.required' => __('Description is required'),
        ];
    }
}
