<?php

namespace App\Imports;

use Exception;
use App\Jobs\CsvImportJob;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CsvImport implements ToCollection, WithHeadingRow
{
    use Importable;

    protected $model;

    private $errors = [];
    private $count = 0;
    private $chunkSize = 1000;

    /**
     * Receives a collection of rows.
     * The maximum for this library to hold is 1000 per batch.
     *
     * @param Illuminate\Support\Collection $collection
     * @return void
     */
    public function collection(Collection $collection)
    {
        try {
            $this->count = $collection->count();
            $collection->chunk($this->chunkSize)->each(function ($rows) {
                CsvImportJob::dispatch(get_class($this), $rows);
            });
        } catch (Exception $e) {
            Log::critical($e->getMessage());
            throw new Exception(__('Unexpected error occurred.'), 500);
        }
    }

    /**
     * Displays the summary of the processed data.
     *
     * @return array
     */
    public function getPostData(): array
    {
        $errorsCount = count($this->errors);

        return [
            'table' => (new $this->model)->getTable(),
            'rows_count' => $this->count,
            'inserted_count' =>  $this->count - $errorsCount ,
            'failed_count' => $errorsCount,
            'failures' => $this->errors
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules(): array
    {
        return [];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    protected function validationMessages(): array
    {
        return [];
    }
}
