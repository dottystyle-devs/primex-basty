<?php

namespace App\Imports;

class StocksImport extends CsvImport
{
    /**
     * Set the model reference.
     */
    public $model = 'App\\Models\\Stock';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'id' => ['required', 'integer'],
            'product_id' => ['required'],
            'on_hand' => ['required', 'integer'],
            'taken' => ['required', 'integer'],
            'production_date' => ['required', 'date'],
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function validationMessages(): array
    {
        return [
            'product_id.required' => __('Product is required.'),
            'on_hand.required' => __('On hand is required.'),
            'taken.required' => __('Description is required'),
            'production_date.required' => __('Production Date is required'),
        ];
    }
}
