<?php

namespace App\Providers;

use App\Services\Contracts\FileContract;
use App\Services\FileService;
use Illuminate\Support\ServiceProvider;

class FileImporterServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FileService::class, function ($app, $params) {
            return new FileService(current($params));
        });
    }
}
