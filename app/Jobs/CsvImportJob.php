<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class CsvImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $importClass;
    private $rows;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($importClass, $rows)
    {
        $this->importClass = $importClass;
        $this->rows = $rows;
    }

    /**
     * Get the importer instance.
     */
    private function importer()
    {
        return new $this->importClass;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $items = $this->rows->map(function ($row) {
            $this->validateRow($row->all());

            return $row->all();
        })->filter()->all();

        $this->importer()->model::upsert($items, ['id']);
    }

    /**
     * Validate each rows and records the failed data.
     *
     * @param array $row
     * @return void
     */
    private function validateRow($row)
    {
        $validator = Validator::make(
            $row,
            $this->importer()->rules(),
            $this->importer()->validationMessages()
        );

        if ($validator->fails()) {
            foreach ($validator->errors()->messages() as $messages) {
                $this->importer()->errors[] = [
                    'id' => $row['id'],
                    'errors' => $messages
                ];
            }
        }
    }
}
