# Product Exam
## System Specifications

| Requirements | Versions |
| :----------: | :------: |
|   LARAVEL    |   8.x    |
|     PHP      | ^7.4.15  |
|    MYSQL     |   ^5.7   |

## Introduction

- Sample import data is found in `storage/samples` directory.
  - This contains huge data 10000+ dummy data generated.
- API success responses are generated using JSON Resources `app/Http/Resources`.
  - These are categorized on how the data is being fetched (e.g return as table, detailed, etc.).
- POSTMAN is used for API Development Tool - Resources can be found in `storage/docs` directory.
  - Import these json files in Postman.
- **ISO8601** is used for date formats.
- Execute `php artisan test` to execute unit testing.

# Sites

|          LOCAL - API           |
| :----------------------------: |
| http://ProductExam.test/api/v1 |
|                                |

# Configuration

1.  Clone this repository.
    ```bash
        $   git clone https://dsc-basty@bitbucket.org/dottystyle-devs/primex-basty.git
    ```
2.  Recreate environment variable file.
    ```bash
        $   cp .env.local .env
    ```
    -   To create the environment variable file for testing **_(optional)_**
    ```bash
        $   cp .env .env.testing
    ```
3.  Install composer
    ```bash
        $   composer install
    ```
4.  Generate Application Key
    ```bash
        $   php artisan key:generate
    ```
5.  Execute Database Migration
    ```bash
        $   php artisan migrate
    ```

# API Overview
## Resopnses
### On Success
#### Show Data
```json
{
  "data" : {
    "id" : int,
    "product" : [
        "id": int,
        ...
    ]
  }
}
```
#### Paginated Data
```json
{
  "data" : {
    [
        "id" : int,
        ...
    ],
    ...
  }
}
```

### On Errors
```json
{
  "code" : int,
  "message" : string
}
```

## Status Codes
| Status Code | Description             |
| :---------- | :---------------------- |
| 200         | `OK`                    |
| 201         | `CREATED`               |
| 400         | `BAD REQUEST`           |
| 404         | `NOT FOUND`             |
| 500         | `INTERNAL SERVER ERROR` |
| 0           | `UNKNOWN`               |

# Development

### Backend Development

1.  Observe Model-View-Controller (MVC) Design Pattern.

2.  Models should store to this path `app\models`.

3.  DB Migration names should starts with [Action]. Make it readable at all times.

    ```
    2014_10_12_000000_create_users_table
    2014_10_12_000000_alter_users_table_add_hobbies_column
    2014_10_12_000000_alter_users_table_remove_motto_column
    2014_10_12_000000_alter_users_table_update_motto_column_to_longText
    2014_10_12_000000_alter_users_table_update_age_column_to_integer
    2014_10_12_000000_alter_users_table_add_sex_default_male
    2014_10_12_000000_alter_users_table_add_index_keys
    2014_10_12_000000_alter_users_table_add_foreign_key_to_tasks_table
    ```

4.  Add comment headers on every functions.

    ```php
        /**
         * Process on importing data.
         *
         * @param string importClass
         * @param \Illuminate\Http\UploadedFile $file
         * @return array $result
         */
        public function import($importClass, UploadedFile $file)
        {
            // Code goes here ...
        }
    ```

# Deployment

| Develop | Staging | Production |
| :-----: | :-----: | :--------: |
| -soon-  | -soon-  |   -soon-   |

# Branching and Versioning

1.  Snake Case is the naming convention for branching.

    > Make sure the changes are relevant with the codes.

    | Issue Number |          Phrase          |    Expected Branch Name     |
    | :----------: | :----------------------: | :-------------------------: |
    |      1       |    create login page     |    #1_create_login_page     |
    |      2       | create user landing page | #2_create_user_landing_page |

2)  Commit Messages Convention.

    > Make the first sentence is the summary of your commit.

    > Make sure the changes are relevant with the codes.

    ```
    Slight modifications on the users table.
    [add] Condition to fetch user by email
    [delete] Condition to fetch user by name
    [update] Add [hobbies - longText] field in user table
    [enhance] Add Logic that can accept dynamic fields
    ```
