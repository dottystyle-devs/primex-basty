<?php

namespace Database\Factories;

use App\Models\Stock;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class StockFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Stock::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $onHand = $this->faker->numberBetween(0, 5);

        return [
            'on_hand' => $onHand,
            'taken' => $this->faker->numberBetween(0, $onHand),
            'production_date' => Carbon::today()->toDateString(),
        ];
    }
}
