<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory  as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Product::factory(20)->create()->each(function ($product) {
            $faker = Faker::create();
            \App\Models\Stock::factory($faker->numberBetween(1,5))->create(['product_id' => $product->id]);
        });
    }
}
