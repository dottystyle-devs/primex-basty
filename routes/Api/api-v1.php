<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StockController;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('products')->group(function () {
    Route::get('/',                 [ProductController::class, 'index'])->middleware('paginated');
    Route::post('/',                [ProductController::class, 'store']);
    Route::post('/import',          [ProductController::class, 'import']);
    Route::get('/{product}',        [ProductController::class, 'show']);
    Route::put('/{product}',        [ProductController::class, 'update']);
    Route::delete('/{product}',     [ProductController::class, 'destroy']);

    Route::post('/{product}/stock', [StockController::class, 'store']);
});

Route::prefix('stocks')->group(function () {
    Route::post('/import',  [StockController::class, 'import']);
});
