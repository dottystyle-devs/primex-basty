**CREATE STOCK**
----
  Returns json data single product.

* **URL**

  `api/v1/products/:PRODUCT_ID/stock`

* **Method:**

  `POST`
  
*  **URL Params**

   **Required:**
 
- `PRODUCT_ID=[int]`
 
   **Optional:**
   
   None

* **Data Params**

   - `on_hand=[string]`
   - `taken=[string]`
   - `production_date=[string]`

* **Success Response:**

  * **Code:** 201 <br />
    **Content:** 
    ```
    {
        "data": {
            "on_hand": "100",
            "taken": "5",
            "production_date": "2021-03-30T00:00:00.000000Z",
            "created_at": "2021-09-22T13:42:06.000000Z",
            "product": {
                "id": 1,
                "code": "n5GPc744vJJIML4g",
                "name": "Hello World!",
                "description": "This Product n5GPc744vJJIML4g is awesome!",
                "created_at": "2021-09-22T08:54:47.000000Z"
            }
        }
    }
    ```
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "code": 404, "message": "Not Found." }`

* **Sample Call:**

  ```javascript
    $.ajax({
      url: "api/v1/products/:PRODUCT_ID/stock",
      dataType: "json",
      type : "POST",
      success : function(r) {
        console.log(r);
      }
    });
  ```
