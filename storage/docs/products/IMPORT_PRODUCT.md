**IMPORT PRODUCT**
----
  Returns json data single product.

* **URL**

  `api/v1/products/import`

* **Method:**

  `POST`
  
*  **URL Params**

   **Required:**

* **Data Params**

   - `file=[file]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```
    {
        "code": 200,
        "message": "Successfully imported data.",
        "data": {
            "table": "products",
            "rows_count": 15000,
            "inserted_count": 15000,
            "failed_count": 0,
            "failures": []
        }
    }
    ```
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "code": 404, "message": "Not Found." }`

  * **Code:** 0 UNKNOWN <br />
    **Content:** `{ "code": 0, "message": "The given data was invalid." }`

* **Sample Call:**

  ```javascript
    $.ajax({
      url: "api/v1/products/import",
      dataType: "json",
      type : "POST",
      success : function(r) {
        console.log(r);
      }
    });
  ```
