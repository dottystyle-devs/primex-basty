**DELETE PRODUCT**
----
  Returns json data single product.

* **URL**

  `api/v1/products/:PRODUCT_ID`

* **Method:**

  `DELETE`
  
*  **URL Params**

   **Required:**
 
   - `PRODUCT_ID=[int]`
 
   **Optional:**
   
   - `stock_id=[integer]`
* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```
    {
        "code": 200,
        "message": "Successfully deleted."
    }
    ```
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "code": 404, "message": "Not Found." }`

  * **Code:** 405 MODEL NOT FOUND <br />
    **Content:** `{ "code": 405, "message": "Model Not Found." }`

* **Sample Call:**

  ```javascript
    $.ajax({
      url: "api/v1/products/16",
      dataType: "json",
      type : "DELETE",
      success : function(r) {
        console.log(r);
      }
    });
  ```
