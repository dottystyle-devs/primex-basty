**ALL PRODUCTS**
----
  Returns json data all products with pagination.

* **URL**

  `api/v1/products`

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   None
 
   **Optional:**
   
   - `per_page=[integer]`
   - `sorts[id]=['asc'|| 'desc']`
   - `filters[code]=[string]`
   - `stock_id=[integer]`
* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```
    {
        "data": [
            {
                "id": 122,
                "code": "NuHThOH3guQ14Q7C",
                "name": "Product NuHThOH3guQ14Q7C",
                "description": "This Product NuHThOH3guQ14Q7C is awesome!",
                "created_at": "2021-09-22T08:54:50.000000Z",
                "onHand_stocks": 3,
                "available_stocks": 1,
                "stocks": [
                    {
                        "id": 344,
                        "on_hand": 0,
                        "taken": 0,
                        "production_date": "2021-09-11T00:00:00.000000Z",
                        "created_at": "2021-09-22T08:54:50.000000Z"
                    }
                ]
            }
        ]
    }
    ```


  * **With `stock_id` Parameter**
  * **Code:** 200 <br />
    **Content:**
    ```
    {
        "data": {
            "on_hand": 0,
            "taken": 0,
            "production_date": "2021-09-11T00:00:00.000000Z",
            "created_at": "2021-09-22T08:54:50.000000Z",
            "product": {
                "id": 122,
                "code": "T5Y2PGM1zNcjkPXI",
                "name": "Product T5Y2PGM1zNcjkPXI",
                "description": "This Product T5Y2PGM1zNcjkPXI is awesome!",
                "created_at": "2021-09-22T08:54:47.000000Z"
            }
        }
    }
    ```
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "code": 404, "message": "Not Found." }`

* **Sample Call:**

  ```javascript
    $.ajax({
      url: "api/v1/products?per_page=10&sorts[id]=asc&sorts[stocks.on_hand]=asc&filters[code]=NuHThOH3guQ14Q7C",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ```
