**SHOW PRODUCT**
----
  Returns json data single product.

* **URL**

  `api/v1/products/:PRODUCT_ID`

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   - `PRODUCT_ID=[int]`
 
   **Optional:**
   
   - `stock_id=[integer]`

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```
    {
        "data": {
            "id": 16,
            "code": "e5BeKQtbN91rZhym",
            "name": "Product e5BeKQtbN91rZhym",
            "description": "This Product e5BeKQtbN91rZhym is awesome!",
            "created_at": "2021-09-22T08:54:47.000000Z",
            "stocks": [
                {
                    "id": 42,
                    "on_hand": 0,
                    "taken": 0,
                    "production_date": "2021-09-11T00:00:00.000000Z",
                    "created_at": "2021-09-22T08:54:50.000000Z"
                },
                {
                    "id": 43,
                    "on_hand": 1,
                    "taken": 0,
                    "production_date": "2021-09-11T00:00:00.000000Z",
                    "created_at": "2021-09-22T08:54:50.000000Z"
                },
                {
                    "id": 44,
                    "on_hand": 5,
                    "taken": 0,
                    "production_date": "2021-09-11T00:00:00.000000Z",
                    "created_at": "2021-09-22T08:54:50.000000Z"
                },
                {
                    "id": 45,
                    "on_hand": 1,
                    "taken": 0,
                    "production_date": "2021-09-11T00:00:00.000000Z",
                    "created_at": "2021-09-22T08:54:50.000000Z"
                }
            ]
        }
    }
    ```


  * **With `stock_id` Parameter**
  * **Code:** 200 <br />
    **Content:**
    ```
    {
        "data": {
            "on_hand": 0,
            "taken": 0,
            "production_date": "2021-09-11T00:00:00.000000Z",
            "created_at": "2021-09-22T08:54:50.000000Z",
            "product": {
                "id": 122,
                "code": "T5Y2PGM1zNcjkPXI",
                "name": "Product T5Y2PGM1zNcjkPXI",
                "description": "This Product T5Y2PGM1zNcjkPXI is awesome!",
                "created_at": "2021-09-22T08:54:47.000000Z"
            }
        }
    }
    ```
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "code": 404, "message": "Not Found." }`

* **Sample Call:**

  ```javascript
    $.ajax({
      url: "api/v1/products/16",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ```
