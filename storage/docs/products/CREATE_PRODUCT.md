**CREATE PRODUCT**
----
  Returns json data single product.

* **URL**

  `api/v1/products`

* **Method:**

  `POST`
  
*  **URL Params**

   **Required:**
   
   None
 
   **Optional:**
   
   None

* **Data Params**

   - `code=[string]`
   - `name=[string]`
   - `description=[string]`
 
* **Success Response:**

  * **Code:** 201 <br />
    **Content:** 
    ```
    {
        "data": {
            "id": 15003,
            "code": "10000004",
            "name": "Product 4",
            "description": "Test Product",
            "created_at": "2021-09-22T12:26:18.000000Z"
        }
    }
    ```
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "code": 404, "message": "Not Found." }`

* **Sample Call:**

  ```javascript
    $.ajax({
      url: "api/v1/products",
      dataType: "json",
      type : "POST",
      success : function(r) {
        console.log(r);
      }
    });
  ```
