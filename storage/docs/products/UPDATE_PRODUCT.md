**UPDATE PRODUCT**
----
  Returns json data single product.

* **URL**

  `api/v1/products/:PRODUCT_ID`

* **Method:**

  `PUT`
  
*  **URL Params**

   **Required:**

   - `PRODUCT_ID=[int]`
 
   **Optional:**
   
   None

* **Data Params - x-www-form-urlencoded**

   - `code=[string]`
   - `name=[string]`
   - `description=[string]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```
    {
        "data": {
            "id": 1,
            "code": "PROD1",
            "name": "Product 1 Updated",
            "description": "Test Product Updated",
            "created_at": "2021-09-22T08:54:47.000000Z"
        }
    }
    ```
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "code": 404, "message": "Not Found." }`

* **Sample Call:**

  ```javascript
    $.ajax({
      url: "api/v1/products/1",
      dataType: "json",
      type : "PUT",
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      success : function(r) {
        console.log(r);
      }
    });
  ```
